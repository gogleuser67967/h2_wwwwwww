const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {InvalidRequestError} = require("../utils/errors");

const {User} = require('../models/userModel');

const registration = async ({username, password}) => {
  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });
  await user.save();
};

const login = async ({username, password}) => {
  const user = await User.findOne({username});

  if (!user) {
    throw new InvalidRequestError('Invalid username or password');
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new InvalidRequestError('Invalid username or password');
  }

  return jwt.sign({
    _id: user._id,
    username: user.username,
  }, process.env.SECRET);
};

module.exports = {
  registration,
  login,
};
